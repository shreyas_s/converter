HomeController = function($scope,$http){
    $scope.values = {
        fromValue : 1
    }
    $scope.items = [];

    $scope.$watch(function(scope){
        return scope.values;
    }, function() {
        console.log( $scope.values )
        var toCurrency = 'INR';
        if( $scope.values.toCurrency == undefined || $scope.values.toCurrency == '' || $scope.values.toCurrency.length != 3 ) {
            // Invalid currency. The user is possibly changing.
            console.log( "Invalid currency" )
        } else {
            console.log( "Valid currency", $scope.values.toCurrency);
        }
        toCurrency = $scope.values.toCurrency;
        fromCurrency = $scope.values.fromCurrency;

        if( $scope.rates != undefined ) {
        	$scope.values.toValue = ( $scope.values.fromValue / $scope.rates[fromCurrency]) * $scope.rates[ toCurrency ] ;
            console.log( $scope.items )
        }


    }, true );
    // $scope.items = [];
	$http.get('http://api.fixer.io/latest?base=USD')
    .success(function(data){
        console.log(data.base)
        console.log(data.rates);
        //console.log(1 * data.rates.INR - data.rates.MXN) 

        $scope.rates = data.rates;

        angular.forEach(data.rates, function(val, key){
            $scope.items.push({
                display: key
            });
        });
    });
	$scope.onSelect = function (item) {
        $scope.values.fromCurrency = item.display
    };

    $scope.fromSelect = function(item){
    	$scope.values.toCurrency = item.display
    };
 /*$scope.movies = ["Lord of the Rings",
                        "Drive",
                        "Science of Sleep",
                        "Back to the Future",
                        "Oldboy"];*/


};
