ViscosityController = function($scope){
	$scope.values = {
        fromValue : 1
    }
    $scope.items = [];
    $scope.values.fromviscosity = 'Newton-second/meter2';
    $scope.$watch(function(scope){
        return scope.values;
    }, function() {
        console.log( $scope.values )
        var toviscosity = 'Newton-second/meter2';
        toviscosity = $scope.values.toviscosity;
        fromviscosity = $scope.values.fromviscosity;
        if( $scope.viscosity != undefined) {
        	$scope.values.toValue = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity[ toviscosity ] ;
            // $scope.values.toValue = $scope.values.fromValue * $scope.viscosity[ toCurrency ];
            console.log(toviscosity)

        }
            $scope.value1 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity['Newton-second/meter2'];
            $scope.value2 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity['kilogramforce-second/meter2'];
            $scope.value3 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity['pascal-second'];
            $scope.value4 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity['millipascal-second'];
            $scope.value5 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity.poise;
            $scope.value6 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity.kilopoise;
            $scope.value7 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity.centipoise;
            $scope.value8 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity.millipoise;
            $scope.value9 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity.decipoise;
            $scope.value10 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity['poundforce-second/foot2'];
            $scope.value11 = ( $scope.values.fromValue / $scope.viscosity[ fromviscosity ] ) * $scope.viscosity['poundforce-second/inch2'];
            
    }, true );

    // $scope.items = [];
    
        //console.log(data.base)
        //console.log(data.viscosity);
        //console.log(1 * data.viscosity.INR - data.viscosity.MXN) 

        $scope.viscosity = {
        'Newton-second/meter2':	1,
		'kilogramforce-second/meter2':	0.1019716213,
		'pascal-second':	1,
		'millipascal-second':	1000,
		'poise':	10,
		'kilopoise':	0.01,
		'decipoise':	100,
		'centipoise':	1000,
		'millipoise':	10000,
		'poundforce-second/foot2':	0.0208854342,
		'poundforce-second/inch2':	0.0001450377



        };
        angular.forEach($scope.viscosity, function(val, key){
            $scope.items.push({
                display: key
            });
        });
    
        
    
        //console.log($scope.viscosity.Ym - $scope.viscosity.dm)
	
	$scope.onSelect = function (item) {
        $scope.values.fromviscosity = item.display
    };

    $scope.fromSelect = function(item){
    	$scope.values.toviscosity = item.display
    };
}