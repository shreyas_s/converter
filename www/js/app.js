// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
var app = angular.module('starter', ['ionic','ngRoute','autocomplete.directive'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})
.factory('Camera', ['$q', function($q) {
 
  return {
    getPicture: function(options) {
      var q = $q.defer();
      
      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);
      
      return q.promise;
    }
  }
}])

.controller('HomeController', HomeController)
.controller('CurrencyController',CurrencyController)
.controller('MeterController',MeterController)
.controller('StorageController',StorageController)
.controller('WeightController',WeightController)
.controller('CamController',CamController)
.controller('AreaController',AreaController)
.controller('TempController',TempController)
.controller('VolumeController',VolumeController)
.controller('TimeController',TimeController)
.controller('SpeedController',SpeedController)
.controller('FuelController',FuelController)
.controller('PressureController',PressureController)
.controller('AcclerationController',AcclerationController)
.controller('AngleController',AngleController)
.controller('velocityController',velocityController)
.controller('SoundController',SoundController)
.controller('PowerController',PowerController)
.controller('TorqueController',TorqueController)
.controller('ViscosityController',ViscosityController)
.controller('FlowrateController',FlowrateController)
.controller('EnergyController',EnergyController)
.controller('MagneticController',MagneticController)
.controller('GridController',GridController)
.controller('velocityController',velocityController)
.controller('GridRepeat',GridRepeat)
.controller('aboutController',aboutController)
.controller('MenuButtonControl',MenuButtonControl);





app.config(function($stateProvider,$urlRouterProvider){
 $urlRouterProvider.otherwise('/grid')

  $stateProvider
   .state('tab',{
    url:'/home',
    templateUrl:'templates/home.html',
    controller:'HomeController'
   })
   .state('currency',{
    url:'/currency',
    templateUrl:'templates/currency.html',
    controller:'CurrencyController'
   })
   .state('meter',{
    url:'/meter',
    templateUrl:'templates/meter.html',
    controller:'MeterController'
   })
   .state('storage',{
    url:'/storage',
    templateUrl:'templates/storage.html',
    controller:'StorageController'
   })
   .state('weight',{
    url:'/weight',
    templateUrl:'templates/weight.html',
    controller:'WeightController'
   })
   .state('camera',{
    url:'/camera',
    templateUrl:'templates/camera.html',
    controller:'CamController'
   })
   .state('area',{
    url:'/area',
    templateUrl:'templates/area.html',
    controller:'AreaController'
   })
   .state('temp',{
    url:'/temp',
    templateUrl:'templates/temperature.html',
    controller:'TempController'
   })
   .state('volume',{
    url:'/volume',
    templateUrl:'templates/volume.html',
    controller:'VolumeController'
   })
   .state('time',{
    url:'/time',
    templateUrl:'templates/time.html',
    controller:'TimeController'
   })
   .state('speed',{
    url:'/speed',
    templateUrl:'templates/speed.html',
    controller:'SpeedController'
   })
   .state('fuel',{
    url:'/fuel',
    templateUrl:'templates/fuel.html',
    controller:'FuelController'
   })
   .state('pressure',{
    url:'/pressure',
    templateUrl:'templates/pressure.html',
    controller:'PressureController'
   })
   .state('acceleration',{
    url:'/acceleration',
    templateUrl:'templates/acceleration.html',
    controller:'AcclerationController'
   })
   .state('angle',{
    url:'/angle',
    templateUrl:'templates/angle.html',
    controller:'AngleController'
   })
   .state('force',{
    url:'/force',
    templateUrl:'templates/force.html',
    controller:'velocityController'
   })
   .state('sound',{
    url:'/sound',
    templateUrl:'templates/sound.html',
    controller:'SoundController'
   })
   .state('power',{
    url:'/power',
    templateUrl:'templates/power.html',
    controller:'PowerController'
   })
   .state('torque',{
    url:'/torque',
    templateUrl:'templates/torque.html',
    controller:'TorqueController'
   })
   .state('viscosity',{
    url:'/viscosity',
    templateUrl:'templates/viscosity.html',
    controller:'ViscosityController'
   })
   .state('flowrate',{
    url:'/flowrate',
    templateUrl:'templates/flowrate.html',
    controller:'FlowrateController'
   })
   .state('energy',{
    url:'/energy',
    templateUrl:'templates/energy.html',
    controller:'EnergyController'
   })
   .state('magnetic',{
    url:'/magnetic',
    templateUrl:'templates/magnetic.html',
    controller:'MagneticController'
   })
   .state('grid',{
    url:'/grid',
    templateUrl:'templates/grid.html',
    controller:'GridController'
   })
   .state('velocity',{
    url:'/velocity',
    templateUrl:'templates/velocity.html',
    controller:'velocityController'
   })
   .state('newgrid',{
    url:'/newgrid',
    templateUrl:'templates/newgrid.html',
    controller:'GridRepeat'
  })
   .state('about',{
    url:'/about',
    templateUrl:'templates/about.html',
    controller:'aboutController'
   })
   .state('menubutton',{
    url:'/menubutton',
    templateUrl:'templates/menuwithbuttons.html',
    controller:'MenuButtonControl'
   })
   

});